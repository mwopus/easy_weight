# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

bl_info = {
	"name": "Easy Weight",
	"author": "Demeter Dzadik",
	"version": (1,0),
	"blender": (2, 90, 0),
	"location": "Weight Paint > Weights > Easy Weight",
	"description": "Operators to make weight painting easier.",
	"category": "3D View"
}
	
import bpy

from . import smart_weight_transfer
from . import force_apply_mirror
from . import toggle_weight_paint
from . import change_brush
from . import weight_paint_context_menu

# Each module is expected to have a register() and unregister() function.
modules = [
	smart_weight_transfer,
	force_apply_mirror,
	toggle_weight_paint,
	change_brush,
	weight_paint_context_menu
]

def register():
	for m in modules:
		m.register()

def unregister():
	for m in modules:
		m.unregister()