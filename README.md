Easy Weight is an addon focused on quality of life improvements for weight painting in Blender.

### Brush Switching
The addon will force-register keybinds for this operator to the 1, 2, 3 keys in Weight Paint mode:  
1: Change to Add Brush.  
2: Change to Subtract Brush.  
3: Change to Blur Brush.  
The brushes must have their default name, ie. "Add", "Subtract", "Blur".  

### Entering Weight Paint Mode
The Toggle Weight Paint Mode operator lets you switch into weight paint mode easier.
Select your object and run the operator.  
- It will find the first armature modifier of your mesh, if there is one. It will ensure the armature is visible and in pose mode.  
- It will set the shading settings to a pure white.  
- If your object's display type was set to Wire, it will set it to Solid.
Run the operator again to restore everything to how it was before.  

I recommend setting up a keybind for this, eg.:  
<img src="docs/toggle_wp_shortcut.png" width="400" />  

### Weight Paint Context Menu
The default context menu (accessed via W key or Right Click) for weight paint mode is not very useful.
The Custom Weight Paint Context Menu operator is intended as a replacement for it.  
<img src="docs/custom_wp_context_menu.png" width="250" />  
_(The "OK" button is not needed, I just can't avoid it)_  

It lets you change the brush falloff type (Sphere/Projected) and the Front Faces Only option. These will affect ALL brushes.  
Also, the color of your brushes will be darker when you're using Sphere falloff.  

I recommend to overwrite the shortcut of the default weight paint context menu like so:  
<img src="docs/wp_context_menu_shortcut.png" width="500" />  

### Toggle Weight Cleaner
This is a new functionality found in the custom WP context menu. When enabled, **this will run the Clean Vertex Groups operator after every brush stroke** while you're in weight paint mode. This means 0-weights are automatically removed as they appear, which helps avoid small weight islands and rogue weights appearing as you work.

### Force Apply Mirror Modifier
In Blender, you cannot apply a mirror modifier to meshes that have shape keys.  
This operator tries to anyways, by duplicating your mesh, flipping it on the X axis and merging into the original. It will also flip vertex groups, shape keys, shape key masks, and even (attempt) shape key drivers, assuming everything is named with .L/.R suffixes.  